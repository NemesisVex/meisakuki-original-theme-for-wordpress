# Meisakuki Original Theme

A custom theme for [Meisakuki](http://gregbueno.com/wp/meisakuki/).

Dependencies
------------

* [VexVox Original Theme](https://bitbucket.org/NemesisVex/vexvox-original-theme-for-wordpress)
* GruntJS
